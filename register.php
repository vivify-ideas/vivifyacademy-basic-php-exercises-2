<?php
	session_start();

	$rawUsers = explode("\n", file_get_contents('users.txt'));

	foreach($rawUsers as $value){
		$existingUsers[] = explode(";", $value);
	}

	if (!empty($_POST)) {
		foreach ($_POST as $key => $value) {
            if (!isset($value) || $value === "") {
                $error = 'Field ' . $key . ' cannot be empty!';
            }
        }

        foreach($existingUsers as $user){
            if($_POST['email'] === $user[2]){
                $error = 'Email ' . $_POST['email'] .  ' is taken! Please select another email address.';
            }
        }

        if($_POST['captcha'] !== $_SESSION['captcha'] && isset($_POST['captcha'])) {
            $error = 'Wrong captcha!';
        } else {
            unset($_POST['captcha']);
        }
	}


	if (!empty($_POST) && empty($error)) {
		$_POST['password'] = crypt($_POST['password']);
        $users = implode($_POST, ";");

		file_put_contents("users.txt", $users . "\n", FILE_APPEND);

		session_unset();

		$_SESSION = [
					'firstName' => $_POST['firstName'],
					'lastName' => $_POST['lastName'],
					'email' => $_POST['email']
				];

		header('Location:home.php');
	}

?>

<?php include 'navigation.php'; ?>

<div class="main">

    <?php if (!empty($error)) : ?>
        <p><?php echo $error ?></p>
    <?php endif; ?>

	<h1>Registration</h1>
	<form class="form" action="" method="POST">
		<div class="form-group">
			<label for="firstName">First Name</label>
			<input type="text" name="firstName">
		</div>
		<div class="form-group">
			<label for="lastName">Last Name</label>
			<input type="text" name="lastName">
		</div>
		<div class="form-group">
			<label for="email">Email</label>
			<input type="email" name="email">
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" name="password">
		</div>
		<div class="form-group">
			<?php
			    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			    $charactersLength = strlen($characters);
			    $randomString = '';
			    for ($i = 0; $i < 6; $i++) {
			        $randomString .= $characters[rand(0, $charactersLength - 1)];
			    }
			    if(empty($_SESSION['captcha'])){
			    	 $_SESSION['captcha'] = $randomString;
			    }
			    if(isset($_SESSION['captcha'])){
			    	unset($_SESSION['captcha']);
					$_SESSION['captcha'] = $randomString;
			    }
			?>
			<label for="captcha" >Captcha: </label>
			<div class="captcha"><?php echo $_SESSION['captcha']; ?></div>
			<input type="text" name="captcha">
		</div> 
		<div class="form-submit">
			<button type="submit">Register</button>
		</div>
	</form>
</div>

<?php include 'footer.php'; ?>