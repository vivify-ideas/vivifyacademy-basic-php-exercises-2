<?php 
	session_start();

	const MAX_LOGIN_ATTEMPTS = 2;

	if (!isset($_SESSION['login_attempts_remaining'])) {
		$_SESSION['login_attempts_remaining'] = MAX_LOGIN_ATTEMPTS;
	}

    // ** povezano sa kodom iz timeout.php - oznacen sa **
	// ovaj deo koda ce se izvrsiti samo ukoliko je korisnik imao 3 neuspela pokusaja
	if ($_SESSION['login_attempts_remaining'] <= 0) {
	    // u sesiji cemo 'upamtiti' vreme kada ce korisnik ponovo moci da pokusa logovanje (trenutno vreme + 300 sec (5min))
		$_SESSION['timeout'] = date('H:i:s', time() + 300);
	}

	// ovo je deo koji sprecava korisnika da poseti login stranu
    // ako nije isteklo 5 min od poslednjeg neuspelog pokusaja
	if ($_SESSION['timeout'] > date('H:i:s')) {
        header('Location:timeout.php');
	}



    // uzimamo registrovane korisnike iz file-a -> niz stringova
    $usersFromFile = explode("\n", file_get_contents('users.txt'));

    // svaki string razbijamo po ';' znaku i smestamo u $users niz.
    // kao rezultat dobijamo niz korisnika (nizova)
    foreach ($usersFromFile as $user) {
        if (isset($user) && $user != "") {
            $users[] = explode(";", $user);
        }
    }


    // ovaj deo koda ce se izvrsiti samo kada je submit-ovana form-a
	if (!empty($_POST)) {

	    // ako korisnik nije uneo email ili password, punimo niz $error sa odgovarajucom greskom
		if (empty($_POST['email'])) {
			$error[] = "Email cannot be empty.";
		}
		else if (empty($_POST['password'])) {
			$error[] = "Password cannot be empty.";
		}

		// ukoliko nema gresaka (korisnik je uneo email i password)
		if (empty($error)) {
			$password = $_POST['password'];

			// iteriramo po user-ima koje smo dobavili iz file-a i uporedjujemo
            // podatke sa forme sa podacima o user-ima iz file-a
			foreach ($users as $value) {
				if ($_POST['email'] === $value[2] && crypt($password, $value[3]) === $value[3]) {
				    // ukoliko smo nasli poklapanje (korisnik je uneo tacne podatke)
                    // upisujemo njegove podatke u sesiju (ulogujemo ga)
					$_SESSION = [
						'firstName' => $value[0],
						'lastName' => $value[1],
						'email' => $value[2]
					];
					//redirekcija na homepage
				    header('Location:home.php');
				}
			}

        $error[] = "Invalid credentials! Please try again.";
		}
	}

	// ukoliko pri loginu dodje do bilo kakve greske
    // obavestavamo korisnika o tome koliko jos pokusaja ima.
	if (!empty($error)){
		$error[] =  $_SESSION['login_attempts_remaining'] . " tries left. You will have to wait 5 minutes before trying again!";
		// smanjujemo broj preostalih pokusaja za jedan
		$_SESSION['login_attempts_remaining']--;
	}
?>

<?php include "navigation.php"; ?>

<div class="main">
	<h1>Login</h1>

    <?php if (!empty($error)) { ?>
        <?php foreach($error as $value) { ?>
            <p class="error"><?php echo $value; ?>
        <?php } ?>
    <?php }?>

	<form class="form" action="" method="POST">
		<div class="form-group">
			<label for="email">Email</label>
			<input type="email" name="email">
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" name="password">
		</div>
		<div class="form-submit">
			<button type="submit">Login</button>
		</div>
	</form>
</div>

<?php 
    include 'footer.php';
?>