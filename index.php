<?php
    session_start();
    include 'navigation.php';
?>

<div class="main">
    <?php if (!empty($_SESSION['firstName'])) { ?>
        <h2>Welcome <?php echo $_SESSION['firstName']; ?></h2>
    <?php } else { ?>
        <h2>Welcome guest</h2>
    <?php } ?>
</div>

<?php include 'footer.php'; ?>