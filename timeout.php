<?php
	session_start();

	// ako je isteklo 5 min od poslednjeg neuspelog pokusaja
    // cistimo sesiju kako bi korisnik mogao da dodje do login strane
    // ** povezano sa kodom iz login.php - oznacen sa **

    // ako se sesija ne bi ocistila, kod u login.php nikad ne bih ispunio uslov:
    // if ($_SESSION['login_attempts_remaining'] <= 0)
    // i uvek bi dodavao novih 5 min na postojece vreme
	if ($_SESSION['timeout'] < date('H:i:s')) {
		session_destroy();
		header('Location:login.php');
	}

?>

<p style="text-align: center">You need to wait <span style="font-weight: bold"><?php echo strtotime($_SESSION['timeout']) - strtotime(date('H:i:s')) ?></span> more seconds before being able to login again</p>

<p style="text-align: center">Please refresh the page to see how much more time you need to wait until you can try to login again.</p>
