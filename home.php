<?php
	session_start();

	$usersFromFile = explode("\n", file_get_contents('users.txt'));

	foreach($usersFromFile as $user) {
		$existingUsers[] = explode(';', $user);
	};

	foreach ($usersFromFile as $user) {
		if (isset($user) && $user != "") {
			$users[] = explode(";", $user);
		}
	}
?>

<?php include "navigation.php"; ?>

<div class="main">
    <h1>All users</h1>
    <?php foreach($users as $key => $user) { ?>
        <p>
            First Name: <?php echo $user[0] ?><br/>
            Last Name: <?php echo $user[1] ?>
        </p>
        <br />
    <?php } ?>
</div>

<?php include 'footer.php'; ?>