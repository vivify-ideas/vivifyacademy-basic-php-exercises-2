<!DOCTYPE html>
<html>
<head>
	<title>PHP Vezbe 2</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<nav class="nav">
        <?php if (!empty($_SESSION['firstName'])) { ?>
            <a href="logout.php">Logout</a>
            <a class="deactivated" href="#">User: <?php echo $_SESSION['firstName'] . " " . $_SESSION['lastName'] ?></a>
        <? } else { ?>
			<a href="login.php">Login</a>
            <a href="register.php">Register</a>
		<?php } ?>
	</nav>